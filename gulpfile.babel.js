import gulp from 'gulp';
import sass from 'gulp-sass';
import sourcemaps from 'gulp-sourcemaps';
import browserSync from 'browser-sync';
import del from 'del';

const paths = {
  src: './src',
  dist: './dist'
}

const cleanUp = () => {
  del(`${paths.dist}/**`);
}

const buildStyles = () => {
  return gulp.src(`${paths.src}/styles/*.scss`)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(paths.dist))
    .pipe(browserSync.stream());
}

const copyMarkup = () => {
  return gulp.src(`${paths.src}/*.html`)
    .pipe(gulp.dest(paths.dist))
    .pipe(browserSync.stream());
}

const copyImages = () => {
  return gulp.src(`${paths.src}/images/**/*`)
    .pipe(gulp.dest(`${paths.dist}/images`))
    .pipe(browserSync.stream());
}

const build = gulp.series(
  buildStyles,
  copyMarkup,
  copyImages
);

const watch = () => {
  gulp.watch(`${paths.src}/**/*.scss`, buildStyles);
  gulp.watch(`${paths.src}/*.html`, copyMarkup);
  gulp.watch(`${paths.src}/images/**/*`, copyImages);
}

const server = () => {
  browserSync.init({
    directory: true,
    ghostMode: {
      clicks: false,
      forms: false,
      scroll: false,
    },
    notify: false,
    open: false,
    reloadDelay: 100,
    server: paths.dist,
  });
}

const serve = gulp.parallel(
  cleanUp,
  watch,
  server
);

const defaultTasks = gulp.parallel(
  build,
  serve
);

export {
  build,
  serve
}

export default defaultTasks;